import numpy as np
import pandas as pd

def similarity(a: np.array, b: np.array):
    num = np.dot(a,b)
    magnitude_a = np.linalg.norm(a)
    magnitude_b = np.linalg.norm(b)
    denom = magnitude_a * magnitude_b
    return num / denom

juniors_list = pd.read_csv('data/ars_juniors.csv')
seniors_list = pd.read_csv('data/ars_seniors.csv')

for idx, junior in juniors_list.iterrows():
    print(junior['email'], '=============')
    junior_vector = junior.iloc[1:].values
    for idx, senior in seniors_list.iterrows():
        senior_vector = senior.iloc[1:].values
        similar = similarity(junior_vector, senior_vector)
        print(similar)