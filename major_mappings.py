mappings = {
	'Accounting & Finance': 201,
	'Aeronautical & Manufacturing Engineering': 451,
	'Agriculture & Forestry': 100,
	'American Studies': 140,
	'Anatomy & Physiology': 166,
	'Anthropology': 901,
	'Archaeology': 902,
	'Architecture': 121,
	'Art & Design': 944,
	'Aural & Oral Sciences': 870,
	'Biomedical Engineering': 454,
	'Biological Sciences': 161,
	'Building': 390,
	'Business & Management Studies': 200,
	'Celtic Studies': 142,
	'Chemical Engineering': 455,
	'Chemistry': 836,
	'Civil Engineering': 456,
	'Classics & Ancient History': 702,
	'Communication & Media Studies': 252,
	'Complementary Medicine': 682,
	'Computer Science': 303,
	'Counselling': 433,
	'Creative Writing': 522,
	'Criminology': 903,
	'Dentistry': 627,
	'Drama': 943,
	'Drama, Dance & Cinematics': 943,
	'Dance & Cinematics': 942,
	'East & South Asian Studies': 142,
	'Economics': 904,
	'Education': 400,
	'Electrical & Electronic Engineering': 459,
	'English': 520,
	'Engineering (General)': 450,
	'Fashion': 945,
	'Film Making': 946,
	'Food Science': 107,
	'Forensic Science': 890,
	'French': 556,
	'Geography & Environmental Sciences': 837,
	'Geology': 837,
	'German': 557,
	'History': 700,
	'History of Art': 941,
	'Architecture & Design': 121,
	'Hospitality': 211,
	'Leisure': 800,
	'Recreation & Tourism': 802,
	'Iberian Languages/Hispanic Studies': 142,
	'Italian': 558,
	'International Relations': 906,
	'Journalism': 254,
	'Land & Property Management': 200,
	'Law': 712,
	'Librarianship & Information Management': 730,
	'Linguistics': 550,
	'Literature': 520,
	'Marketing': 218,
	'Materials Technology': 466,
	'Mathematics': 742,
	'Mechanical Engineering': 467,
	'Medical Technology': 617,
	'Medicine': 628,
	'Middle Eastern & African Studies': 142,
	'Music Performance': 952,
	'Music Studies': 950,
	'Music Studies (Composition, history, etc)': 950, 
	'Nursing': 619,
	'Occupational Therapy': 678,
	'Optometry': 680,
	'Ophthalmology & Orthoptics': 680,
	'Pharmacology & Pharmacy': 629,
	'Philosophy': 821,
	'Physics and Astronomy': 833,
	'Physiotherapy': 682,
	'Politics': 907,
	'Psychology': 870,
	'Robotics': 516,
	'Russian & East European Languages': 560,
	'Social Policy': 883,
	'Social Work': 884,
	'Sociology': 908,
	'Sports Science': 603,
	'Theology & Religious Studies': 822,
	'Town & Country Planning and Landscape Design': 123,
	'Veterinary Medicine': 630,
	'Youth Work': 884,
	'NaN': '999'
}

general_areas = {
	'Agriculture': 100,
	'Business': 200,
	'Engineering': 450,
	'General Arts (not to be confused with social sciences or liberal arts)': 940,
	'Health and Medicine': 600,
	'Law': 710,
	'Liberal Arts': 720,
	'Multi-/Interdisciplinary Studies': 770,
	'Performing Arts': 940,
	'Public and Social Services': 880,
	'Sciences': 830,
	'Social Sciences': 900,
	'Technology (Computer science, data science, etc)': 300,
	'Technology': 300,
	'Trades and Personal Services': 390,
	'NaN': 999
}