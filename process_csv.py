import pandas as pd
import major_mappings
import race_types
import country_mappings
import fileinput
import time


def plus_one(x):
    return x + 1

def class_name(st):
    if st == "Junior (Grade 11)":
        return 11
    else:
        return 12

def location_app(loc: str):
    location_sum = 0
    all_locations = loc.split(', ')
    for location in all_locations:
        if location == 'US schools':
            location_sum += 2
        elif location == 'Canadian schools':
            location_sum += 4
        elif location == "UK schools":
            location_sum += 10
        elif location == 'International schools':
            location_sum += 14
    return location_sum


def numerical_pairing_prefs(pref: str):
    if pref == "Yes, I know what I want to/am studying and I want/have specialized advice!":
        return 1
    elif pref == "Sort of, I want someone in the general area (E.g. physics majoring senior wants someone going into pure sciences)":
        return 20
    else:
        return 300


def major_to_sat_code(major):
    if major != major:
        return major_mappings.mappings['NaN']
    return major_mappings.mappings[major]


def gen_area_to_code(area):
    if area != area:
        return major_mappings.general_areas['NaN']
    return major_mappings.general_areas[area]


def gen_area_list(lst):
    if lst != lst:
        return gen_area_to_code(lst)
    areas_sum = 0
    all_areas = lst.split(', ')
    for area in all_areas:
        areas_sum += gen_area_to_code(area)
    return areas_sum


def row_wise_social_media(row):
    row['reddit'] = int("reddit" in row['platforms'].lower())
    print(row)

def intl_region_codes(region):
    if region != region:
        return -1

def finaid_numerical(aid_desc):
    if aid_desc != aid_desc:
        return 3
    aid = aid_desc.lower()
    if 'yes' in aid or 'maybe' in aid:
        return 1
    elif 'no' in aid or 'none' in aid:
        return 2
    else:
        return 3

def gender_to_number(gen):
    if gen != gen:
        return 4
    gender = gen.lower()
    if gender == 'male':
        return 1
    elif gender == 'female':
        return 2
    elif gender == 'trans':
        return 3
    elif gender == 'neutral':
        return 4
    else:
        return 5

def gender_id_to_number(id):
    if id != id:
        return 8
    identity = id.lower()
    if identity == 'straight':
        return 1
    elif identity == 'gay/lesbian':
        return 5
    elif identity == 'bi-sexual':
        return 6
    elif identity == 'asexual':
        return 7
    elif identity == 'lgbtq2+, somewhere on the spectrum':
        return 8

def school_sys_to_number(sch):
    if sch != sch:
        return 8
    school = sch.lower()
    if school == 'ap':
        return 1
    elif school == 'ib':
        return 2
    elif school == 'a-levels':
        return 3
    elif school ==  'ig':
        return 4
    elif school == 'ecf':
        return 5
    elif '100 point scale' in school:
        return 6
    elif 'gpa' in school:
        return 7
    else:
        return 8

def intl_region_to_number(reg):
    if reg != reg:
        return 0
    else:
        return country_mappings.countries[reg]

df = pd.read_csv('data/arsrespv3.csv')
df.columns = ['email',
              'grade',
              'residency',
              'app_loc',
              'field_proximity',
              'major',
              'gen_area',
              'availability',
              'platforms',
              'race',
              'gender',
              'gender_id',
              'finaid',
              'types',
              'intl_region',
              'school_sys']

# drop dupes
df.drop_duplicates(subset='email', inplace=True)

# change some unparsable output
df.replace("Technology (Computer science, data science, etc)", "Technology", inplace=True)

# class to numerical
df['grade'] = df['grade'].map(class_name)

# residency to numerical
residency_types = ['U.S. Resident', 'Canadian Resident', 'International Resident']
df['residency'] = df['residency'].astype('category', categories=residency_types).cat.codes
df['residency'] = df['residency'].map(plus_one)

# app_loc to numerical
df['app_loc'] = df['app_loc'].map(location_app)

# field_proximity to numerical
df['field_proximity'] = df['field_proximity'].map(numerical_pairing_prefs)

# major to numerical
df['major'] = df['major'].map(major_to_sat_code)

# gen_area to numerical
df['gen_area'] = df['gen_area'].map(gen_area_list)

# row-wise social media
for media in ['reddit', 'email', 'discord', 'skype', 'whatsapp', 'cellphone', 'insta']:
    df[f'sm_{media}'] = df['platforms'].apply(lambda x: int(media in x.lower()))
df = df.drop(labels=['platforms'], axis=1)

# applying to...
for category in ['t20', 'public', 'private', 'lac']:
    df[f'applying_to_{category}'] = df['types'].apply(lambda x: int(x == x) and int(category in x.lower()))
df = df.drop(labels=['types'], axis=1)

# intl region
df['finaid'] = df['finaid'].map(finaid_numerical)

# gender
df['gender'] = df['gender'].map(gender_to_number)

# gender identity
df['gender_id'] = df['gender_id'].map(gender_id_to_number)

# race
for race in race_types.types:
    df[f'race_{race_types.column_mappings[race]}'] = df['race'].apply(lambda x: int(x == x) and int(race in x.lower()))
df = df.drop(labels=['race'], axis=1)

# school system
df['school_sys'] = df['school_sys'].map(school_sys_to_number)

# intl country
df['intl_region'] = df['intl_region'].map(intl_region_to_number)



# output email list and processed datasets for import
# file name has current timestamp appended
cur_time = int(time.time())

df['email'].to_csv(f'data/processed/emails_{cur_time}.csv', index=False)
df.loc[df['grade'] == 11].to_csv(f'data/processed/ars_juniors_{cur_time}.csv', index=False)
df.loc[df['grade'] == 12].to_csv(f'data/processed/ars_seniors_{cur_time}.csv', index=False)
df.to_csv(f'data/processed/ars_processed_{cur_time}.csv', index=False)