# Adopt A Rising Senior

This is the codebase for the Adopt-A-Rising-Senior algorithm. Currently everything is unstable and subject to change.

## How to Run

### Raw Data to Numerical
1. Download the spreadsheet from Google Sheets as a CSV.
2. Clone this repository.
3. Put the data in a new folder `data/` inside the cloned repo. Rename the CSV `arsrespv3.csv`.
4. `mkdir ./data/processed` to create a place for the processed data to go.
5. Create a `virtualenv` to run the program: `virtualenv env && source env/bin/activate`
6. `pip install -r requirements.txt`
7. `python process_csv.py`: This should produce a new CSV file called `ars_processed.csv`. This is the processed file.